// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <bitset>
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <tchar.h>
#include <assert.h>
#include <exception>
#include <cstdio>

using namespace std;
#if defined(WIN32)
#include <windows.h>
#endif
#include <cryptopp/rsa.h>
using CryptoPP::RSA;
using CryptoPP::InvertibleRSAFunction;
using CryptoPP::RSAES_OAEP_SHA_Encryptor;
using CryptoPP::RSAES_OAEP_SHA_Decryptor;
#include <cryptopp/sha.h>
using CryptoPP::SHA1;
#include <cryptopp/osrng.h>
using CryptoPP::AutoSeededRandomPool;
#include <cryptopp/files.h>
using CryptoPP::FileSink;
using CryptoPP::FileSource;
#include <cryptopp/cryptlib.h>
using CryptoPP::Exception;
using CryptoPP::DecodingResult;
#include <cryptopp/elgamal.h>
using CryptoPP::ElGamal;
using CryptoPP::ElGamalKeys;
#include <cryptopp/gost.h>
using CryptoPP::GOST;
using CryptoPP::SecByteBlock;
using CryptoPP::ArraySink;
using CryptoPP::ArraySource;
#include <cryptopp/modes.h>
using CryptoPP::CBC_Mode;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;
using CryptoPP::StringSink;

#include <opencv/highgui.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc/imgproc.hpp>
using namespace cv;

#include "GOST.h"
#include "ELGAMAL.h"
#include "RSA.h"
#include "encrypt.h"
#include "decrypt.h"
#include "Histogram.h"
#include "stego.h"


const string RSA_PUBKEY = "RSAPub.key";
const string RSA_PVTKEY = "RSAPvt.key";
const string EG_PUBKEY = "EGPub.key";
const string EG_PVTKEY = "EGPvt.key";
const string GT_PUBKEY = "GTPub.key";
const string GT_PVTKEY = "GTPvt.key";
const string IN_IMG_PATH = "D:/stego/input/";
const string HISTO_PATH = "D:/stego/histogram/";
const string OUT_IMG_PATH = "D:/stego/output/";
const string KEY_PATH = "D:/stego/key/";

void print_exetime(clock_t start, clock_t stop);


