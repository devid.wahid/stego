#include "stdafx.h"
#include "decrypt.h"
#include "stego.h"
int dekripsi(int kripto, string kripto_str, string file_gambar)
{
	system("cls");
	cout << "======================================" << endl;
	cout << "Pilihan Kriptografi: " << kripto_str << endl;
	
	string extractedmsg;
	imageToString(file_gambar, kripto_str, extractedmsg);
	
	RSA::PrivateKey rsa_pvtkey;
	ElGamalKeys::PrivateKey eg_pvtkey;
	string rsa_decrypt_result;	
	string eg_decrypt_result;
	string gost_decrypt_result;
	
	SecByteBlock gost_key(GOST::MAX_KEYLENGTH);
	byte gost_iv[8];
	switch (kripto) {
		case 1:							
			cout << "Lokasi Private Key: " << KEY_PATH + RSA_PVTKEY << endl;			
			load_key(rsa_pvtkey, KEY_PATH + RSA_PVTKEY);
			cout << "======================================" << endl;			
			cout << "- Hasil ekstraksi pesan dari gambar:" << endl;
			cout << extractedmsg << endl;
			Decrypt(rsa_pvtkey, extractedmsg, rsa_decrypt_result);
			cout << "======================================" << endl;
			cout << "- Hasil dekripsi:" << endl;

			cout << rsa_decrypt_result << endl;
			cout << "======================================" << endl;
			break;
		case 2:
			cout << "Lokasi Private Key: " << KEY_PATH + EG_PVTKEY << endl;			
			load_key(eg_pvtkey, KEY_PATH + EG_PVTKEY);
			cout << "======================================" << endl;			
			cout << "- Hasil ekstraksi pesan dari gambar:" << endl;
			cout << extractedmsg << endl;
			Decrypt(eg_pvtkey, extractedmsg, eg_decrypt_result);
			cout << "======================================" << endl;
			cout << "- Hasil dekripsi:" << endl;
			cout << eg_decrypt_result << endl;
			cout << "======================================" << endl;
			break;
		case 3:			
			cout << "======================================" << endl;			
			cout << "- Hasil ekstraksi pesan dari gambar:" << endl;
			cout << extractedmsg << endl;
			FileSource fs("D:/stego/key/GTPvt.key", true, new ArraySink(gost_key.begin(), gost_key.size()));
			FileSource fs2("D:/stego/key/GTPub.key", true, new ArraySink(gost_iv, sizeof(gost_iv)));
			Decrypt(gost_key, gost_iv, extractedmsg, gost_decrypt_result);
			cout << "======================================" << endl;
			cout << "- Hasil dekripsi:" << endl;
			cout << gost_decrypt_result << endl;
			cout << "======================================" << endl;
			break;
	}
	cout.precision(2);
	Mat	oldim = imread(IN_IMG_PATH + file_gambar.c_str()); //image matrix
	Mat	newim = imread(OUT_IMG_PATH + file_gambar.c_str()); //image matrix
	cout << "- PSNR = " << fixed << getPSNR(newim, oldim) << endl;

	return 1;
}