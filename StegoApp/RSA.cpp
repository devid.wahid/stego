#include "stdafx.h"

void save_key(RSA::PublicKey& key, const string& filepath)
{
	key.Save(
		FileSink(filepath.c_str(), true /*binary*/).Ref()
		);
	cout << "Disimpan di: " << filepath << " | ";
}
void load_key(RSA::PublicKey& key, const string& filepath)
{
	key.Load(
		FileSource(filepath.c_str(), true, NULL, true /*binary*/).Ref()
		);
	cout << "======================================" << endl;
	cout << "Kunci " << filepath << " berhasil dimuat." << endl;
}
void save_key(RSA::PrivateKey& key, const string& filepath)
{
	key.Save(
		FileSink(filepath.c_str(), true /*binary*/).Ref()
		);
	cout << "Disimpan di: " << filepath << " | ";
}
void load_key(RSA::PrivateKey& key, const string& filepath)
{
	key.Load(
		FileSource(filepath.c_str(), true, NULL, true /*binary*/).Ref()
		);
	cout << "======================================" << endl;
	cout << "Kunci " << filepath << " berhasil dimuat." << endl;
}
void rsa_keygen() {
	//Generate Random Number
	AutoSeededRandomPool rng;
	//Create Parameter (randNum, KeySize)
	InvertibleRSAFunction parameters;
	parameters.GenerateRandomWithKeySize(rng, 1024);

	cout << "(1/2) Membuat Kunci Privat " << endl;
	clock_t start = clock();
	RSA::PrivateKey privateKey(parameters);
	save_key(privateKey, KEY_PATH + RSA_PVTKEY);
	clock_t stop = clock();
	print_exetime(start, stop);
	cout << "======================================" << endl;
	clock_t start1 = clock();
	cout << "(2/2) Membuat Kunci Publik ... " << endl;
	RSA::PublicKey publicKey(parameters);
	save_key(publicKey, KEY_PATH + RSA_PUBKEY);
	clock_t stop1 = clock();
	print_exetime(start1, stop1);
}
void Encrypt(RSA::PublicKey publicKey, string msg, string& cipher)
{
	try{
	// Encrypt
	AutoSeededRandomPool rng;
	RSAES_OAEP_SHA_Encryptor encryptor(publicKey);

	// Create cipher text space
	CryptoPP::StringSource ss1(msg, true, new CryptoPP::PK_EncryptorFilter(rng, encryptor, new CryptoPP::StringSink(cipher)));
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl << "CryptoPP::Exception caught:" << endl
			<< e.what() << endl;
	}
}

void Decrypt(RSA::PrivateKey privateKey, string msg, string& recovered)
{
	try {
		AutoSeededRandomPool rng;
		RSAES_OAEP_SHA_Decryptor decryptor(privateKey);
		// Create recovered text space
		CryptoPP::StringSource ss2(msg, true, new CryptoPP::PK_DecryptorFilter(rng, decryptor, new CryptoPP::StringSink(recovered)));
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl <<"================================" << endl<<"Dekripsi gagal:" << endl
			<< e.what() << endl;	
	}
}

