#include "stdafx.h"
void gost_keygen() {
	AutoSeededRandomPool rnd;

	// Generate a random key
	SecByteBlock key(GOST::MAX_KEYLENGTH);
	rnd.GenerateBlock(key, key.size());
	
	// Generate a random IV
	byte iv[GOST::BLOCKSIZE];
	rnd.GenerateBlock(iv, GOST::BLOCKSIZE);
	cout << "(1/2) Membuat Kunci Privat " << endl;
	clock_t start = clock();
	ArraySource as1(key, sizeof(key), true, new FileSink("D:/stego/key/GTPvt.key"));
	clock_t stop = clock();
	print_exetime(start, stop);
	cout << "======================================" << endl;
	clock_t start1 = clock();
	cout << "(2/2) Membuat Kunci Publik ... " << endl;
	ArraySource as2(iv, sizeof(iv), true, new FileSink("D:/stego/key/GTPub.key"));
	clock_t stop1 = clock();
	print_exetime(start1, stop1);
}

void Encrypt(SecByteBlock key, byte iv[], string plain, string& cipher) {
	try{
	CBC_Mode<GOST>::Encryption e;
	e.SetKeyWithIV(key, key.size(), iv);
	StringSource ss(plain, true,
		new StreamTransformationFilter(e,
			new StringSink(cipher)
			) // StreamTransformationFilter      
		);
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl << "CryptoPP::Exception caught:" << endl
			<< e.what() << endl;
	}
}
void Decrypt(SecByteBlock key, byte iv[], string cipher, string& recovered){
	try{
	CBC_Mode<GOST>::Decryption d;
	d.SetKeyWithIV(key, key.size(), iv);

	// The StreamTransformationFilter removes
	//  padding as required.
	StringSource ss(cipher, true,
		new StreamTransformationFilter(d,
			new StringSink(recovered)
			) // StreamTransformationFilter
		); // StringSource
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl << "================================" << endl << "Dekripsi gagal:" << endl
			<< e.what() << endl;
	}
}