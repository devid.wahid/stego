#include "stdafx.h"

int getHistogram(string filename, string savelocation) {
	Mat src, dst;
	src = imread(filename.c_str());

	/// Separate the image in 3 places ( B, G and R )
	vector<Mat> bgr_planes;
	split(src, bgr_planes);

	/// Establish the number of bins
	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };

	bool uniform = true; bool accumulate = false;

	Mat b_hist, g_hist, r_hist;

	/// Compute the histograms:
	calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);

	// Draw the histograms for B, G and R
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

	/// Normalize the result to [ 0, histImage.rows ]
	
	normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
	normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

	/// Draw for each channel
	for (int i = 1; i < histSize; i++)
	{
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(b_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(b_hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(g_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(g_hist.at<float>(i))),
			Scalar(0, 255, 0), 2, 8, 0);
		line(histImage, Point(bin_w*(i - 1), hist_h - cvRound(r_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(r_hist.at<float>(i))),
			Scalar(0, 0, 255), 2, 8, 0);
	}

	double b_maxVal, g_maxVal, r_maxVal = 0;
	minMaxLoc(b_hist, 0, &b_maxVal, 0, 0);
	minMaxLoc(g_hist, 0, &g_maxVal, 0, 0);
	minMaxLoc(r_hist, 0, &r_maxVal, 0, 0);
	
	imwrite(savelocation, histImage);
	waitKey(0);
	cout << "======================================" << endl;
	cout << "File disimpan di "<< savelocation << endl;
	cout << "Max B hist: " << b_maxVal << endl;
	cout << "Sum B hist: " << sum(b_hist).val[0] << endl;
	cout << "Max G hist: " << g_maxVal << endl;
	cout << "Sum G hist: " << sum(g_hist).val[0] << endl;
	cout << "Max R hist: " << r_maxVal << endl;
	cout << "Sum R hist: " << sum(r_hist).val[0] << endl;

	cout << "======================================" << endl;
	return 0;
}