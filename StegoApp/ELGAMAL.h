#pragma once
void elgamal_keygen();
void save_key(ElGamalKeys::PublicKey& key, const string& filepath);
void save_key(ElGamalKeys::PrivateKey& key, const string& filepath);
void load_key(ElGamalKeys::PublicKey& key, const string& filepath);
void load_key(ElGamalKeys::PrivateKey& key, const string& filepath);
void Encrypt(ElGamalKeys::PublicKey publicKey, string msg, string& cipher);
void Decrypt(ElGamalKeys::PrivateKey privateKey, string msg, string& recovered);
