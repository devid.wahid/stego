#pragma once
void rsa_keygen();
void save_key(RSA::PublicKey& key, const string& filepath);
void save_key(RSA::PrivateKey& key, const string& filepath);
void load_key(RSA::PublicKey& key, const string& filepath);
void load_key(RSA::PrivateKey& key, const string& filepath);
void Encrypt(RSA::PublicKey publicKey, string msg, string& cipher);
void Decrypt(RSA::PrivateKey privateKey, string msg, string& recovered);