// stdafx.cpp : source file that includes just the standard includes
// StegoApp.pch will be the pre-compiled header
// stdafx.obj will contain the pre-compiled type information

#include "stdafx.h"

void print_exetime(clock_t start, clock_t stop)
{
	double elapsed = (double)(stop - start) * 1000.0 / CLOCKS_PER_SEC;
	cout << "Waktu eksekusi: " << elapsed << " ms" << endl;
}