#pragma once
int stringToImage(string encryptedMsg, string img_name, string chipper, char encryptlength);
void imageToString(string filename, string chipper, string& extractedmsg);
double getPSNR(const Mat& I1, const Mat& I2);