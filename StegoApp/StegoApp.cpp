// StegoApp.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
void pilih_enkripsi()
{

	string input_gambar;
	string input_pesan;
	do {
		system("cls");
		cout << "Masukkan pesan rahasia anda:" << endl;
		getline(cin, input_pesan);
		if (input_pesan.empty()) {
			cout << "Pesan tidak boleh kosong" << endl;
		}
	} while (input_pesan.empty());
	int select_gambar;
	system("cls");
	cout << "Pilih Gambar:" << endl;
	cout << "1. flower_01.jpg" << endl;
	cout << "2. flower_02.jpg" << endl;
	cout << "3. flower_03.bmp" << endl;
	cout << "4. flower_04.bmp" << endl;
	cout << "5. flower_05.png" << endl;
	cout << "6. flower_06.png" << endl;
	cin >> select_gambar;
	switch (select_gambar) {
	case 1:
		input_gambar = "flower_01.jpg";
		break;
	case 2:
		input_gambar = "flower_02.jpg";
		break;
	case 3:
		input_gambar = "flower_03.bmp";
		break;
	case 4:
		input_gambar = "flower_04.bmp";
		break;
	case 5:
		input_gambar = "flower_05.png";
		break;
	case 6:
		input_gambar = "flower_06.png";
		break;
	}

KRIPTO:
	int kripto;
	cout << "======================================" << endl;
	cout << "Pesan Rahasia: " << input_pesan << endl;
	cout << "Gambar: " << IN_IMG_PATH + input_gambar << endl;

	cout << "======================================" << endl;
	cout << "Pilih Metode Kriptografi!" << endl;
	cout << "1. RSA" << endl;
	cout << "2. EL-GAMAL" << endl;
	cout << "3. GOST" << endl;
	cin >> kripto;
	switch (kripto) {
	case 1:
		enkripsi(kripto, "RSA", input_pesan, input_gambar);
		break;
	case 2:
		enkripsi(kripto, "EL-GAMAL", input_pesan, input_gambar);
		break;
	case 3:
		enkripsi(kripto, "GOST", input_pesan, input_gambar);
		break;
	default:
		system("cls");
		cout << "======================================" << endl;
		cout << "Pilihan tidak valid" << endl;
		cin.get();
		goto KRIPTO;
		break;
	}
}

void pilih_histogram() {
	string input_gambar;
	
	int select_gambar;
	system("cls");
	cout << "Pilih Gambar:" << endl;
	cout << "1. flower_01.jpg" << endl;
	cout << "2. flower_02.jpg" << endl;
	cout << "3. flower_03.bmp" << endl;
	cout << "4. flower_04.bmp" << endl;
	cout << "5. flower_05.png" << endl;
	cout << "6. flower_06.png" << endl;
	cin >> select_gambar;
	switch (select_gambar) {
	case 1:
		input_gambar = "flower_01.jpg";
		break;
	case 2:
		input_gambar = "flower_02.jpg";
		break;
	case 3:
		input_gambar = "flower_03.bmp";
		break;
	case 4:
		input_gambar = "flower_04.bmp";
		break;
	case 5:
		input_gambar = "flower_05.png";
		break;
	case 6:
		input_gambar = "flower_06.png";
		break;
	}
		
	getHistogram(IN_IMG_PATH+input_gambar,HISTO_PATH+"before_"+input_gambar);
	getHistogram(OUT_IMG_PATH + input_gambar, HISTO_PATH + "after_" + input_gambar);
}

void pilih_dekripsi()
{
	string input_gambar;
	int select_gambar;
		system("cls");
		cout << "Pilih Gambar:" << endl;
		cout << "1. flower_01.jpg" << endl;
		cout << "2. flower_02.jpg" << endl;
		cout << "3. flower_03.bmp" << endl;
		cout << "4. flower_04.bmp" << endl;
		cout << "5. flower_05.png" << endl;
		cout << "6. flower_06.png" << endl;
		cin >> select_gambar;
		switch (select_gambar) {
			case 1:
				input_gambar = "flower_01.jpg";
				break;
			case 2:
				input_gambar = "flower_02.jpg";
				break;
			case 3:
				input_gambar = "flower_03.bmp";
				break;
			case 4:
				input_gambar = "flower_04.bmp";
				break;
			case 5:
				input_gambar = "flower_05.png";
				break;
			case 6:
				input_gambar = "flower_06.png";
				break;
		}
	

KRIPTO:
	int kripto;
	cout << "======================================" << endl;
	cout << "Gambar: " << OUT_IMG_PATH + input_gambar << endl;
	cout << "======================================" << endl;
	cout << "Pilih Metode Kriptografi!" << endl;
	cout << "1. RSA" << endl;
	cout << "2. EL-GAMAL" << endl;
	cout << "3. GOST" << endl;
	cin >> kripto;
	switch (kripto) {
	case 1:
		dekripsi(kripto, "RSA", input_gambar);
		break;
	case 2:
		dekripsi(kripto, "EL-GAMAL", input_gambar);
		break;
	case 3:
		dekripsi(kripto, "GOST", input_gambar);
		break;
	default:
		system("cls");
		cout << "======================================" << endl;
		cout << "Pilihan tidak valid" << endl;
		cin.get();
		goto KRIPTO;
		break;

	}
}
int main()
{
PESAN:
	int aksi;
	cout << endl;
	cout << ":: Steganography ::" << endl;
	cout << "1. Enkripsi pesan" << endl;
	cout << "2. Dekripsi pesan" << endl;
	cout << "3. Buat Plot Histogram" << endl;
	cout << "" << endl;
	cin >> aksi;
	switch (aksi)
	{
	case 1:
		pilih_enkripsi();
		goto PESAN;
		break;
	case 2:
		pilih_dekripsi();
		goto PESAN;
		break;
	case 3:
		pilih_histogram();
		goto PESAN;
		break;
	default:
		cout << endl;
		cout << "Pilihan tidak terdaftar" << endl;
		cout << endl;
		cin.get();
		goto PESAN;
		break;
	}
	cin.get();
	return 0;
}

