#include "stdafx.h"
#include "encrypt.h"
#include "stego.h"
int enkripsi(int kripto, string kripto_str, string textToEncrypt, string file_gambar)
{
	
	system("cls");
	cout << "======================================" << endl;
	cout << "Pilihan Kriptografi: ";
	cout << kripto_str << endl;
	cout << "======================================" << endl;
	RSA::PublicKey rsa_pubkey;
	string rsa_encrypt_result;
	ElGamalKeys::PublicKey elgamal_pubkey;
	string elgamal_encrypt_result;
	string gost_encrypt_result;
	SecByteBlock gost_key(GOST::MAX_KEYLENGTH);
	byte gost_iv[8];
	switch (kripto) {
	case 1:
		rsa_keygen();
		load_key(rsa_pubkey, KEY_PATH + RSA_PUBKEY);
		Encrypt(rsa_pubkey, textToEncrypt, rsa_encrypt_result);
		cout << "======================================" << endl;
		cout << "Enkripsi Pesan" << endl;
		cout << "- Pesan sebelum enkripsi: " << textToEncrypt << endl;
		cout << "- Pesan terenkripsi: " << rsa_encrypt_result << endl;
		cout << "- Lebar enkripsi : " << rsa_encrypt_result.length() << endl;
		cout << "======================================" << endl;
		cout << "Menyembunyikan pesan terenkripsi dalam gambar" << endl;		
		stringToImage(rsa_encrypt_result, file_gambar,kripto_str, (char) rsa_encrypt_result.length());
		
		break;
	case 2:
		elgamal_keygen();
		load_key(elgamal_pubkey, KEY_PATH + EG_PUBKEY);
		Encrypt(elgamal_pubkey, textToEncrypt, elgamal_encrypt_result);
		cout << "======================================" << endl;
		cout << "Enkripsi Pesan" << endl;
		cout << "- Pesan sebelum enkripsi: " << textToEncrypt << endl;
		cout << "- Pesan terenkripsi: " << elgamal_encrypt_result << endl;
		cout << "- Lebar enkripsi : " << elgamal_encrypt_result.length() << endl;
		cout << "======================================" << endl;
		cout << "Menyembunyikan pesan terenkripsi dalam gambar" << endl;
		stringToImage(elgamal_encrypt_result, file_gambar, kripto_str, (char)elgamal_encrypt_result.length());
		break;
	case 3:
		gost_keygen();
		FileSource fs("D:/stego/key/GTPvt.key", true, new ArraySink(gost_key.begin(), gost_key.size()));
		FileSource fs2("D:/stego/key/GTPub.key", true, new ArraySink(gost_iv, sizeof(gost_iv)));
		cout << "======================================" << endl;
		cout << "Kunci D:/stego/key/GTPvt.key berhasil dimuat." << endl;
		Encrypt(gost_key, gost_iv,textToEncrypt, gost_encrypt_result);
		cout << "======================================" << endl;
		cout << "Enkripsi Pesan" << endl;
		cout << "- Pesan sebelum enkripsi: " << textToEncrypt << endl;
		cout << "- Pesan terenkripsi: " << gost_encrypt_result << endl;
		cout << "- Lebar enkripsi : " << gost_encrypt_result.length() << endl;
		cout << "======================================" << endl;
		cout << "Menyembunyikan pesan terenkripsi dalam gambar" << endl;
		stringToImage(gost_encrypt_result, file_gambar, kripto_str, (char)gost_encrypt_result.length());
		break;
	}
	return 1;
}
