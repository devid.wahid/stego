#include "stdafx.h"

float correlation(Mat &m1, Mat &m2) {

	Mat m1_float, m2_float, correl;
	float sumX=0, sumY=0, sumXY = 0;
	float squareSum_X = 0, squareSum_Y = 0;
	float n = 0;
	for (int i = 0; i < m1.rows; i++) {
		for (int j = 0; j < m1.cols; j++) {			
			sumX += (float) sum(m1.at<Vec3b>(i, j))[0];
			sumY += (float) sum(m2.at<Vec3b>(i, j))[0];
			sumXY += (float) (sum(m1.at<Vec3b>(i, j))[0] * sum(m2.at<Vec3b>(i, j))[0]);
			n += 1;
		}
	}
	cout << "sumX: " << sumX <<endl;
	cout << "sumY: " << sumY << endl;
	cout << "sumXY: " << sumXY << endl;
	float corr = (float)(n * sumXY - sumX * sumY)
		/ sqrt((n * squareSum_X - sumX * sumX)
			* (n * squareSum_Y - sumY * sumY));
	return corr;
}

double countAvalancheEffect(const Mat& Img1, const Mat& Img2) {
	double bit_change = 0;
	double total_bit = 0;
	for (int y = 0; y< Img1.rows; y++) {
		for (int x = 0;x<Img1.cols;x++) {
			//BGR		
			int i1 = Img1.at<Vec3b>(x, y)[0] + Img1.at<Vec3b>(x, y)[1] + Img1.at<Vec3b>(x, y)[2];
			int i2 = Img2.at<Vec3b>(x, y)[0] + Img2.at<Vec3b>(x, y)[1] + Img2.at<Vec3b>(x, y)[2];
			if (i1 != i2) {
				bit_change += 1;
			}
			total_bit += 1;
		}
	}
	cout << "Bit Change: " << bit_change << endl;
	cout << "Total Bit: " << total_bit << endl;
	double avalanche = bit_change/total_bit *100;
	return avalanche;
}
double getPSNR(const Mat& I1, const Mat& I2)
{
	Mat s1;
	absdiff(I1, I2, s1);       // |I1 - I2|
	s1.convertTo(s1, CV_32F);  // cannot make a square on 8 bits
	s1 = s1.mul(s1);           // |I1 - I2|^2

	Scalar s = sum(s1);         // sum elements per channel

	double sse = s.val[0] + s.val[1] + s.val[2]; // sum channels

	if (sse <= 1e-10) // for small values return zero
		return 0;
	else
	{
		double  mse = sse / (double)(I1.channels() * I1.total());
		double psnr = 10.0*log10((255 * 255) / mse);
		return psnr;
	}
}
void irregularDeviation(const Mat& Im1, const Mat& Im2) {
	Mat Di, histD;
	Mat hsv1, hsv2;
	cvtColor(Im1, hsv1, CV_BGR2HSV);
	cvtColor(Im2, hsv2, CV_BGR2HSV);
	absdiff(hsv1, hsv2, Di);       // D = |I1 - I2|
	// Establish the number of bins
	int histSize = 256;

	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 };
	const float* histRange = { range };
	bool uniform = true; bool accumulate = false;
	int channels[] = { 0, 1,2 };
	calcHist(&Di, 1, channels, Mat(), histD, 1, &histSize, &histRange, uniform, accumulate); // H = Histogram(D)

	Scalar sumHist = sum(histD);	
	double DC =  sumHist.val[0]/256;	//DC
	Mat Hi, AC;	
	subtract(histD, DC, Hi); // H(i)-DC
	absdiff(Hi, DC, AC); // AC(i) = | H(i)-DC |
	Scalar ID = sum(AC); //ID
	cout << "- Deviasi Ketidakateraturan= " << ID.val[0] << endl;

}
void maxDeviation(const Mat& Im1, const Mat& Im2) {
	Mat hsv1, hsv2;
	cvtColor(Im1 , hsv1, CV_BGR2HSV);
	cvtColor(Im2, hsv2, CV_BGR2HSV);
	// Quantize the hue to 30 levels
	// and the saturation to 32 levels
	int hbins = 30, sbins = 32;
	int histSize[] = { hbins, sbins };
	// hue varies from 0 to 179, see cvtColor
	float hranges[] = { 0, 180 };
	// saturation varies from 0 (black-gray-white) to
	// 255 (pure spectrum color)
	float sranges[] = { 0, 256 };
	const float* ranges[] = { hranges, sranges };
	MatND hist1, hist2;
	// we compute the histogram from the 0-th and 1-st channels
	int channels[] = { 0, 1 ,2};
	calcHist(&hsv1, 1, channels, Mat(), // do not use mask
		hist1, 2, histSize, ranges,
		true, // the histogram is uniform
		false);
	calcHist(&hsv2, 1, channels, Mat(), // do not use mask
		hist2, 2, histSize, ranges,
		true, // the histogram is uniform
		false);
	Scalar Im1_hist = sum(hist1);
	Scalar Im2_hist = sum(hist2);
	//cout << "Sum hist im1: " << Im1_hist.val[0] << endl;
	//cout << "Sum hist im2: " << Im2_hist.val[0] << endl;
	Mat s1;
	absdiff(hist1, hist2, s1);       // |I1 - I2|
	
	Scalar s = sum(s1);
	//cout << "Amplitude of the absolute difference: "<< s << endl;
	cout << "- Maximum Deviation (D) = " << (Im1_hist.val[0]/ 2) + (s.val[0]) << endl;
	

}

struct bufstate_e {
	char byte;
	unsigned bitsLeft;
	string msg;
	int i;
};
uchar mergeNextTwoBits(bufstate_e& bs, uchar q)
{
	bitset<8> result;
	bitset<8> pixel(q);

	if (bs.bitsLeft == 0) {		
		if(bs.i < bs.msg.length()){
			
		//if (in.get(bs.byte)) {			
			bs.bitsLeft = 8;
			bs.byte = bs.msg.at(bs.i);
			//cout << "Read byte: " << bitset<8>(bs.byte) << " " << int(bs.byte) << " " << static_cast<char>(int(bs.byte)) << endl;
		}
		else {
			result = bitset<8>(q);
			//cout<<"TEST:"<< result<< endl;
		}
		bs.i += 1;
	}

	if (bs.bitsLeft > 0) {
		bs.bitsLeft -= 2;
		result = (bs.byte & (3 << bs.bitsLeft)) >> bs.bitsLeft;
		//cout << "bitsLeft="<< bs.bitsLeft <<" "<< result<<endl;
	}

	result = (result << 8) | result;

	//menggabungkan binary pixel asli dengan binary pesan 
	bitset<8> bit6(252);
	bitset<8> lsb2 = (pixel & bit6);
	bitset<8> merged = lsb2 | result;
	
	
	//cout << "Next two: " << bitset<8>(result) << " merged into " << hex << pixel << " is " << merged << endl;
	return static_cast<char>(int(merged.to_ulong()));
}


int stringToImage(string encryptedMsg, string filename, string chipper, char encryptlength) {
	
	clock_t startStegoE = clock();	
	string ext = ".tiff"; //Tambahkan .tiff untuk format tiff
	Mat	image = imread(IN_IMG_PATH + filename.c_str(), IMREAD_COLOR); //image matrix
	Mat oldim, newim;
	image.copyTo(oldim);
	
	bufstate_e bs = { 0, 0 ,encryptlength+encryptedMsg,0};
	
	for(int y=0; y< image.rows; y++){
		for(int x=0;x<image.cols;x++){	
			//BGR			
			image.at<Vec3b>(x, y)[0] = mergeNextTwoBits(bs, image.at<Vec3b>(x, y)[0]);			
			image.at<Vec3b>(x, y)[1] = mergeNextTwoBits(bs, image.at<Vec3b>(x, y)[1]);
			image.at<Vec3b>(x, y)[2] = mergeNextTwoBits(bs, image.at<Vec3b>(x, y)[2]);
			
		}
	}
	image.copyTo(newim);
	//Write Image	
	vector<int> compression_params;
	compression_params.push_back(IMWRITE_PNG_COMPRESSION);
	compression_params.push_back(0);
	compression_params.push_back(IMWRITE_JPEG_QUALITY);
	compression_params.push_back(100);

	string before = OUT_IMG_PATH + filename + ext;
	string after = OUT_IMG_PATH + filename;
	
	remove(after.c_str());
	imwrite(OUT_IMG_PATH + filename + ext, newim, compression_params);
	rename(before.c_str(), after.c_str());
	clock_t stopStegoE = clock();		
	cout << "- Stego berhasil, gambar disimpan di " << OUT_IMG_PATH << " | ";
	print_exetime(startStegoE, stopStegoE);
	cout.precision(5);
	cout << "- PSNR = " << fixed << getPSNR(oldim,newim) <<endl;
	cout << "- AVALANCHE = " << countAvalancheEffect(oldim, newim) << "%" << endl;
	cout << "- CORRELATION = " << correlation(oldim, newim) << endl;
	maxDeviation(oldim, newim);
	irregularDeviation(oldim, newim);
	return 1;
};

struct bufstate_d {
	unsigned char byte;
	unsigned bits;
	int x;
	string msg;
	int keysize;
};
void pushTwoBits(bufstate_d& bs, uchar q) {	
	bitset<8> pix(bitset<8>(q) & bitset<8>(3));
	char bits = static_cast<char>(int(pix.to_ulong()));
	
	bs.byte <<= 2;
	bs.byte |= bits;
	bs.bits += 2;
	if (bs.bits == 8) {
		//cout << bs.byte;
		bs.x += 1;
		if (bs.x == 1) {
			bs.keysize = int(bs.byte);
			cout << "Key Size: " << bs.keysize <<endl;
		}
		if(bs.x>1 && bs.x <= (bs.keysize + 1)){
		//if (bitset<8>(bs.byte) != bitset<8>(0)) {
			//cout << "Dumping byte " << bitset<8>(bs.byte) << " "<< int(bs.byte)<<" "<<bs.x <<endl;
			bs.msg += bs.byte;
		}	
		bs.byte = 0;
		bs.bits = 0;		
	}

}

void imageToString(string filename, string chipper, string& extractedmsg) {
	clock_t startStegoD = clock();
	
	cout << "======================================" << endl;
	Mat	image = imread(OUT_IMG_PATH + filename.c_str()); //image matrix
	cout << "Gambar : "<<OUT_IMG_PATH + filename.c_str() << endl;
	
	bufstate_d bs = { 0, 0 ,0,"", 0};
	for (int y = 0; y< image.rows; y++) {
		for (int x = 0;x<image.cols;x++) {
			//BGR
			pushTwoBits(bs, image.at<Vec3b>(x, y)[0]);
			pushTwoBits(bs, image.at<Vec3b>(x, y)[1]);
			pushTwoBits(bs, image.at<Vec3b>(x, y)[2]);

		}
	}
		
	extractedmsg = bs.msg;
	clock_t stopStegoD = clock();
	cout << "======================================" << endl;
	print_exetime(startStegoD, stopStegoD);	
}