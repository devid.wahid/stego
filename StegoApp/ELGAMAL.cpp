#include "stdafx.h"

void save_key(ElGamalKeys::PublicKey& key, const string& filepath)
{
	key.Save(
		FileSink(filepath.c_str(), true /*binary*/).Ref()
		);
	cout << "Disimpan di: " << filepath << " | ";
}
void load_key(ElGamalKeys::PublicKey& key, const string& filepath)
{
	key.Load(
		FileSource(filepath.c_str(), true, NULL, true /*binary*/).Ref()
		);
	cout << "======================================" << endl;
	cout << "Kunci " << filepath << " berhasil dimuat." << endl;
}
void save_key(ElGamalKeys::PrivateKey& key, const string& filepath)
{
	key.Save(
		FileSink(filepath.c_str(), true /*binary*/).Ref()
		);
	cout << "Disimpan di: " << filepath << " | ";
}
void load_key(ElGamalKeys::PrivateKey& key, const string& filepath)
{
	key.Load(
		FileSource(filepath.c_str(), true, NULL, true /*binary*/).Ref()
		);
	cout << "======================================" << endl;
	cout << "Kunci " << filepath << " berhasil dimuat." << endl;
}
void elgamal_keygen() {
	//Generate Random Number
	AutoSeededRandomPool rng;

	cout << "(1/2) Membuat Kunci Privat " << endl;
	clock_t start = clock();
	ElGamal::Decryptor decryptor;
	decryptor.AccessKey().GenerateRandomWithKeySize(rng, 512); //default 2048
	ElGamalKeys::PrivateKey& privateKey = decryptor.AccessKey();
	save_key(privateKey, KEY_PATH + EG_PVTKEY);
	clock_t stop = clock();
	print_exetime(start, stop);

	cout << "======================================" << endl;

	clock_t start1 = clock();
	cout << "(2/2) Membuat Kunci Publik ... " << endl;
	ElGamal::Encryptor encryptor(decryptor);
	ElGamalKeys::PublicKey& publicKey = encryptor.AccessKey();
	save_key(publicKey, KEY_PATH + EG_PUBKEY);
	clock_t stop1 = clock();
	print_exetime(start1, stop1);
}
void Encrypt(ElGamalKeys::PublicKey publicKey, string msg, string& cipher)
{
	try {
		AutoSeededRandomPool rng;
		ElGamal::Encryptor encryptor(publicKey);

		CryptoPP::StringSource ss1(msg, true, new CryptoPP::PK_EncryptorFilter(rng, encryptor, new CryptoPP::StringSink(cipher)));
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl << "CryptoPP::Exception caught:" << endl
			<< e.what() << endl;
	}
}

void Decrypt(ElGamalKeys::PrivateKey privateKey, string msg, string& recovered)
{
	try {
		AutoSeededRandomPool rng;
		ElGamal::Decryptor decryptor(privateKey);

		CryptoPP::StringSource ss2(msg, true, new CryptoPP::PK_DecryptorFilter(rng, decryptor, new CryptoPP::StringSink(recovered)));
	}
	catch (CryptoPP::Exception const& e)
	{
		cout << endl << "================================" << endl << "Dekripsi gagal:" << endl
			<< e.what() << endl;
	}

}
